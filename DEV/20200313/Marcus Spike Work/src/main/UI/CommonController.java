package main.UI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Word;

import java.io.IOException;

public abstract class CommonController {
    static public ObservableList<Word> dictionaryWords;

    /**
     * A method that changes the scene
     *
     * @param filePath is the path to to the new file
     * @param b        is the button used to overwrite the current scene
     * @throws IOException if the filepath is wrong
     */
    @FXML
    public void changeScene(String filePath, Button b) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource(filePath));
        Parent root = loader.load();

        Scene scene = new Scene(root);
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void showPopUp(String filePath, String title) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource(filePath));
        Parent root = loader.load();

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(scene);
        stage.show();
    }


    /**
     * A method that returns a list of words.
     * Should get words from the dictionary (JSON)
     *
     * @return a list of all the words
     */
    public ObservableList<Word> getWords() {
            dictionaryWords = FXCollections.observableArrayList();
            dictionaryWords.add(new Word("Hello", "Helo"));
            dictionaryWords.add(new Word("University", "Prifysgol"));
            dictionaryWords.add(new Word("Food", "Bwyd"));
        return dictionaryWords;

    }
}
