package main.UI;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class FlashcardController extends CommonController implements Initializable {
    private int timesFlipped = 0;
    @FXML
    private Button flashcard;
    @FXML
    private Button previousButton;
    @FXML
    private Button goToPractice;
    @FXML
    private Button nextButton;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    //Initialize the first word in the practice list
    }

    @FXML
    private void flipFlashcard() {
        timesFlipped++;
        if(timesFlipped%2 == 0){
            flashcard.setText("ENGLISH WORD");
        } else if(timesFlipped%2 == 1){
            flashcard.setText("WELSH WORD");
        }
    }

    @FXML
    private void showNextFlashcard() {
        //Go to the next word in the practice list
        flashcard.setText("NEXT WORD");
    }

    @FXML
    private void showPreviousFlashcard() {
        //Go to the previous word in the practice list
        flashcard.setText("PREVIOUS WORD");
    }

    @FXML
    private void goToPractice() throws IOException {
        String filePath = "../../PracticeView.fxml";
        changeScene(filePath, goToPractice);
    }
}
