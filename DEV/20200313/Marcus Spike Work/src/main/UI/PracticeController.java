package main.UI;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import main.Word;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PracticeController extends CommonController implements Initializable {
    @FXML
    private Button goToFlashcards;
    @FXML
    private Button goToTest;
    @FXML
    private Button goToDictionary;
    @FXML
    private TableView<Word> tableViewDictionary;
    @FXML
    private TableColumn<Word, String> englishColumn;
    @FXML
    private TableColumn<Word, String> welshColumn;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        englishColumn.setCellValueFactory(new PropertyValueFactory<Word, String>("englishWord"));
        welshColumn.setCellValueFactory(new PropertyValueFactory<Word, String>("welshWord"));

        tableViewDictionary.setItems(getWords());
    }


    @FXML
    private void goToDictionary() throws IOException {

        String filePath = "../../DictionaryView.fxml";
        changeScene(filePath, goToDictionary);
    }

    @FXML
    private void goToFlashcards() throws IOException {
        String filePath = "../../Flashcard.fxml";
        changeScene(filePath, goToFlashcards);
    }

    @FXML
    private void goToTest() throws IOException {

        String filePath = "../../CompareTest.fxml";
        changeScene(filePath, goToDictionary);
    }

}
