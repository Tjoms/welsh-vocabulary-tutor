package main.UI;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

import java.io.IOException;

public class HelpController extends CommonController{
    @FXML
    private Button goToDictionary;

    //needs to get some information displayed (currently navigation medthod: Tree (Empty))
    @FXML
    private void goToDictionary() throws IOException {
        String filePath = "../../DictionaryView.fxml";
        changeScene(filePath, goToDictionary);
    }
}
