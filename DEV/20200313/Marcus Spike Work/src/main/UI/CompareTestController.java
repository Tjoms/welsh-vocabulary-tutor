package main.UI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CompareTestController extends CommonController implements Initializable {
    @FXML
    private Button continueButton;
    @FXML
    private ChoiceBox choiceBoxA;
    @FXML
    private ChoiceBox choiceBoxB;
    @FXML
    private ChoiceBox choiceBoxC;
    @FXML
    private ChoiceBox choiceBoxD;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<String> choices = FXCollections.observableArrayList();
        choices.add("A");
        choices.add("B");
        choices.add("C");
        choices.add("D");

        choiceBoxA.setItems(choices);
        choiceBoxB.setItems(choices);
        choiceBoxC.setItems(choices);
        choiceBoxD.setItems(choices);
    }

    @FXML
    private void continueTest() throws IOException {
        String filePath = "../../TypeTest.fxml";
        changeScene(filePath, continueButton);
    }
}
