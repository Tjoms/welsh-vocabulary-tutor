package main.UI;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

import java.io.IOException;

public class MultipleChoiceTestController extends  CommonController{
    @FXML
    private Button continueButton;

    @FXML
    private void continueTest() throws IOException {
        String filePath = "../../PracticeView.fxml";
        changeScene(filePath, continueButton);
    }
}
