package main.UI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import main.Word;


import java.net.URL;
import java.util.ResourceBundle;

public class AddWordController extends CommonController implements Initializable {

    @FXML
    private Button cancel;
    @FXML
    private Button confirm;
    @FXML
    private TextField englishTranslation;
    @FXML
    private TextField welshTranslation;
    @FXML
    private ChoiceBox typeOfWord;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Maybe add genders to noun
        ObservableList<String> typeOfWords = FXCollections.observableArrayList();
        typeOfWords.add("NOUN");
        typeOfWords.add("ADJECTIVE");
        typeOfWords.add("VERB");
        typeOfWords.add("ADVERB");
        typeOfWords.add("PRONOUN");
        typeOfWord.setItems(typeOfWords);

    }

    @FXML
    private void closeWindow() {
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void confirmAction() {
        Stage stage = (Stage) confirm.getScene().getWindow();
        String english = englishTranslation.getText();
        String welsh = welshTranslation.getText();
        String type = (String)typeOfWord.getValue();
        Word newWord = new Word(english, welsh, type);

        dictionaryWords.add(newWord);

        //should save to JSON and update practice list.
        stage.close();
    }
}