public class Word {
    private String english, welsh;
    private Gender gender;
    private Type type;
    private boolean favourite;


    public Word(String english, String welsh, Gender gender, Type type) {
        this.english = english;
        this.welsh = welsh;
        this.gender = gender;
        this.type = type;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getWelsh() {
        return welsh;
    }

    public void setWelsh(String welsh) {
        this.welsh = welsh;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        if(type.equals(Type.NOUN)||type.equals(Type.ADJECTIVE))
        this.gender = gender;
        else this.gender=null;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }
}
