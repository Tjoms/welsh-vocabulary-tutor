import java.util.ArrayList;

import java.util.TreeMap;

public class Logic {

    boolean englishToWelsh;//Defines the preference of the dictionary. 0= WelshToEnglish, 1= EnglishToWelsh
    TreeMap<String, ArrayList<Word>> dictionary; //Contains the dictionary.
    ArrayList<Word> favouriteWords;

    /**
     * Returns the translation of a specific word.
     *
     * @param word
     * @return
     */
    private String translation(String word) {
        if (englishToWelsh)
            return dictionary.get(word).get(0).getWelsh(); //TODO: More than one translation admited?
        return dictionary.get(word).get(0).getEnglish();
    }

    /**
     * Looks for a word starting with the given input.
     *
     * @param search
     */
    private void searchWord(String search) {
        if (englishToWelsh) {
            dictionary.forEach((s, word) ->
                    {
                        if (s.startsWith(search)) {
                            System.out.println(s + " - " + word.get(0).getWelsh());
                            //TODO: More than one translation showed? (Although this wont work like this when UI is integrated).
                        }

                    }

            );
        }
        dictionary.forEach((s, word) ->
                {
                    if (s.startsWith(search)) {
                        System.out.println(s + " - " + word.get(0).getEnglish());
                    }

                }
        );

    }

    //I would be really surprised if this works. Ill try to explain the method.
    private void addWord(String english, String welsh, Type type, Gender gender) {
        Word newWord = new Word(english, welsh, gender, type);
        ArrayList<Word> newList; //New list is created to replace the old one.
        if (englishToWelsh) {
            newList = dictionary.get(english);// access the ArrayList with the given Key. May fail if it does not exist :D
            newList.add(newWord); //adds the new Word to the previous list.
            dictionary.put(english, newList); // PUTS THE WORD :D
        }
        newList = dictionary.get(english);
        newList.add(newWord);
        dictionary.put(english, newList);
}


    /**
     * Displays the map alphabetically.
     */
    private void displayAlphabetically() {
        if (englishToWelsh)
            dictionary.forEach((s, word) -> System.out.println(s + " - " + word.get(0).getWelsh()));
        else
            dictionary.forEach((s, word) -> System.out.println(s + " - " + word.get(0).getEnglish()));
    }

    /**
     * Displays the map in reverse order
     */
    private void displayReversed() {
        if (englishToWelsh)
            dictionary.descendingMap().forEach((s, word) -> System.out.println(s + " - " + word.get(0).getWelsh()));
        else
            dictionary.descendingMap().forEach((s, word) -> System.out.println(s + " - " + word.get(0).getEnglish()));

    }

    /**
     * Loads the new dictionary.
     */
    // private void loadDictionary()
    //{
    //  Util util= new Util();

    //if(englishToWelsh) dictionary=util.loadEnglish();
    //else dictionary=util.loadWelsh();

    //}

    /**
     * Method that should be called when adding the words and should automatically add to the list the favourite ones.
     */
    private void updateFavouriteWords() {
        ArrayList<Word> newList= new ArrayList<>();
        dictionary.values().forEach(words -> {
            words.forEach(word -> {
                if (word.isFavourite()) newList.add(word);
            });
        });
        favouriteWords=newList;
    }
}