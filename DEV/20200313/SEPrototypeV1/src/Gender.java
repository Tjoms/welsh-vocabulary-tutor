public enum Gender {
    MASCULINE,
    FEMININE,
    COMMON,
    NEUTRAL
}
