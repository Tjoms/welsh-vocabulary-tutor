package uk.aber.ac.uk;

import org.json.simple.JSONObject;

import java.util.*;

public class Dictionary implements IDictionary {

    private TreeMap<String, Word> words = new TreeMap<>();
    private Lang ordering = Lang.EN;

    @Override
    public void put(Word word) {
        words.put(word.getEnglish(), word);
    }

    private String[] splitPlurals(String coded) {
        String[] decoded = {coded, ""};
        if (coded.contains("/")) {
            decoded = coded.split("\\"+"/");
            if (decoded[0].contains("(")) {
                String[] stripped = decoded[0].split(" ");
                stripped = Arrays.copyOf(stripped, stripped.length - 1);
                decoded[1] = String.join(" ", stripped) + decoded[1];
                decoded[0] = decoded[0] + ")";
                System.out.println(Arrays.toString(decoded));
            } else if (Math.abs(decoded[0].length() - decoded[1].length()) >= 5) {
                decoded[1] = decoded[0] + decoded[1];
            }
            return decoded;
        }
        decoded[1] = decoded[0];
        return decoded;
    }

    @Override
    public void put(JSONObject word) {
        String codedEnglish = (String) word.get("english");
        String codedWelsh = (String) word.get("welsh");
        String[] english = splitPlurals(codedEnglish);
        String[] welsh = splitPlurals(codedWelsh);
        String[] mainLang;
        if (ordering == Lang.EN) {
            mainLang = english;
        } else {
            mainLang = welsh;
        }
        for (int i = 0; i < 2; i++) {
            words.put(mainLang[i], new Word(english[i], welsh[i], Type.valueOf((String) word.get("wordType"))));
        }
    }

    public void reorder(String language) {
        TreeMap<String, Word> temp = null;
        if (language.equalsIgnoreCase("cy") && ordering != Lang.CY) {
            temp = new TreeMap<>();
            for (Map.Entry<String, Word> entry : words.entrySet()) {
                Word word = entry.getValue();
                temp.put(word.getWelsh(), word);
            }
            ordering = Lang.CY;
        }
        if (language.equalsIgnoreCase("en") && ordering != Lang.EN) {
            temp = new TreeMap<>();
            for (Map.Entry<String, Word> entry : words.entrySet()) {
                Word word = entry.getValue();
                temp.put(word.getEnglish(), word);
            }
            ordering = Lang.EN;
        }
        if (temp != null) {
            words = temp;
            System.out.println("BLAH");
        }
    }

    @Override
    public void reverse() {
        TreeMap<String, Word> temp = new TreeMap<>(Collections.reverseOrder());
        temp.putAll(words);
        words = temp;
    }

    public void fave(Word word, boolean bool) {
        if (ordering == Lang.EN) {
            words.get(word.getEnglish()).setFavourite(bool);
        } else {
            words.get(word.getWelsh()).setFavourite(bool);
        }
    }

    private String verbify(Word s) {
        if (s.getWordType() == Type.verb) {
            return "to ";
        }
        return "";
    }

    public void display() {
        if (ordering == Lang.EN) {
            words.forEach((s, word) -> System.out.println(verbify(word) + s + " - " + word.getWelsh() + " : " + word.getWordType()));
        } else {
            words.forEach((s, word) -> System.out.println(s + " - " + word.getEnglish() + " : " + word.getWordType()));
        }
    }

    /**
     * Looks for a word starting with the given input.
     *
     * @param search word/word part to search for
     */
    @Override
    public ArrayList<Word> search(String search) {
        ArrayList<Word> matches = new ArrayList<>();
        words.forEach((s, word) ->
                {
                    if (s.startsWith(search)) {
                        matches.add(word);
                    }
                }
        );
        return matches;
    }

}
