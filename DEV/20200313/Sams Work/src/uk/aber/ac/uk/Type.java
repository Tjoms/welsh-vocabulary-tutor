package uk.aber.ac.uk;

/**
 * Possible types of words
 *
 * nm = noun masculine
 * nf = noun feminine
 * v/V = verb
 *
 */
public enum Type {
    nm, verb, nf, other
}
