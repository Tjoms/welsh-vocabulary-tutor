package uk.aber.ac.uk;

import org.json.simple.JSONObject;

import java.util.ArrayList;

/**
 * generic data structure interface, specifies functions required from our data structures for the dictionary
 *
 * this is the factory design pattern
 */

public interface DataStructure{
    void put(Word word);
    void put(JSONObject word);
    void display();
}
