package uk.aber.ac.uk;


public class Match {

    private Word word;
    private Lang language;


    public Match(Word word, Lang lang){
        this.word = word;
        language = lang;
    }

    public String showEnglish(){
        return word.getEnglish();
    }

    public String showWelsh(){
        return word.getWelsh();
    }

    /**
     *
     * @param answer the answer in english or welsh
     * @return if the answer matches the translation
     */
    public boolean isCorrect(String answer){
        if (language==Lang.EN){
            return (answer.equalsIgnoreCase(word.getWelsh()));
        }
        return (answer.equalsIgnoreCase(word.getEnglish()));
    }



}
