package uk.aber.ac.uk;


import java.util.ArrayList;

public interface IDictionary extends DataStructure {
    void reorder(String string);
    ArrayList<Word> search(String word);
    void reverse();
    void fave(Word word, boolean bool);
}
