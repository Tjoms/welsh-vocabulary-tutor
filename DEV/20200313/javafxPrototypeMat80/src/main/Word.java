package main;

public class Word {
    private String englishWord;
    private String welshWord;
    private String typeOfWord;

    public Word(String englishWord, String welshWord){
        this.englishWord = englishWord;
        this.welshWord = welshWord;
    }

    public Word(String englishWord, String welshWord, String typeOfWord){
        this.englishWord = englishWord;
        this.welshWord = welshWord;
        this.typeOfWord = typeOfWord;
    }
    public String getEnglishWord() {
        return englishWord;
    }

    public String getWelshWord() {
        return welshWord;
    }

    public void setTypeOfWord(String typeOfWord) {
        this.typeOfWord = typeOfWord;
    }

    public String getTypeOfWord() {
        return typeOfWord;
    }
}
