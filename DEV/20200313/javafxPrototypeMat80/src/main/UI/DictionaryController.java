package main.UI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import main.Word;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class DictionaryController extends CommonController implements Initializable {
    private boolean englishSorted = true;
    private boolean welshSorted = false;


    @FXML
    private TableView<Word> tableViewDictionary;
    @FXML
    private TableColumn<Word, String> englishColumn;
    @FXML
    private TableColumn<Word, String> welshColumn;
    @FXML
    private Button goToPractice;
    @FXML
    private Button addToDictionary;
    @FXML
    private Button goToHelp;
    @FXML
    private TextField search;


    /**
     * A method that initializes everything before the scene is displayed
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        englishColumn.setCellValueFactory(new PropertyValueFactory<Word, String>("englishWord"));
        welshColumn.setCellValueFactory(new PropertyValueFactory<Word, String>("welshWord"));

        tableViewDictionary.setItems(getWords());
    }

    /**
     * A method that adds a word to the dictionary.
     * Should be implemented to edit JSON file
     */
    @FXML
    private void addToDictionary() throws IOException {
        String filePath = "../../AddWordPopUp.fxml";
        String title = "Add word";
        showPopUp(filePath, title);



//        Word milk = new Word("Milk", "Llaeth");
//        dictionaryWords.add(milk);
//        tableViewDictionary.setItems(dictionaryWords);
    }

    /**
     * A method that changes the scene to the help scene
     */
    @FXML
    private void goToHelp() throws IOException {
        String filePath = "../../HelpView.fxml";
        changeScene(filePath, goToHelp);
    }

    /**
     * A method that changes the scene to the practice scene
     * @throws IOException
     */
    @FXML
    private void goToPractice() throws IOException {
        //should save JSON file before continuing...
        String filePath = "../../PracticeView.fxml";
        changeScene(filePath, goToPractice);
    }

    /**
     * A method that search in the dictionary and displays the results in the dictionary view
     * If there is nothing in the search bar, all the words in the dictionary is displayed
     */
    @FXML
    private void updateDictionary() {
        Word currentWord = null;
        ObservableList<Word> searchedWords = FXCollections.observableArrayList();
        String searchWord = search.getText().toLowerCase();
        System.out.println(searchWord);
        int wordIndex = 0;
        do {
            currentWord = null;
            if (wordIndex < dictionaryWords.size()) {
                currentWord = dictionaryWords.get(wordIndex);
                if (englishSorted) {
                    if (currentWord != null && currentWord.getEnglishWord().toLowerCase().startsWith(searchWord)) {
                        searchedWords.add(currentWord);
                    }
                } else if (welshSorted) {
                    if (currentWord != null && currentWord.getWelshWord().toLowerCase().startsWith(searchWord)) {
                        searchedWords.add(currentWord);
                    }
                }
                wordIndex++;
            }
        } while (currentWord != null);
        tableViewDictionary.setItems(searchedWords);
        if (searchedWords == null) {
            tableViewDictionary.setItems(dictionaryWords);
        }
    }

    /**
     * A method that prints out the sorted column and changes the state of the program
     *
     * If English is sorted, English = true
     * If Welsh is sorted, Welsh = true
     */
    @FXML
    private void updateSortedList() {
        try {
            TableColumn sortColumn = tableViewDictionary.getSortOrder().get(0);

            if (sortColumn.getId().contains("englishColumn")) {
                englishSorted = true;
                welshSorted = false;
            } else if (sortColumn.getId().contains("welshColumn")) {
                englishSorted = false;
                welshSorted = true;
            }

            System.out.println(sortColumn.getId());
        } catch (RuntimeException e) {
            System.out.println("No columns sorted, using previous selected coloumn as selceted");
        }
    }
}
