package ui.java.fx.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class StackPaneController {
	
	@FXML
	public void openHelp () {
		try {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(this.getClass().getResource("/fxml/HelpWindow.fxml"));
		StackPane stackPane = loader.load();
		Scene scene = new Scene(stackPane);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.setTitle("HELP");
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
		
		} catch (Exception e) {
			System.out.println("Can't load new window");
		}
		

	}
	
	@FXML
	public void openAddWord () {
		try {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(this.getClass().getResource("/fxml/AddNewWord.fxml"));
		StackPane stackPane = loader.load();
		Scene scene = new Scene(stackPane);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.setTitle("Add Word");
		stage.setResizable(false);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
		
		} catch (Exception e) {
			System.out.println("Can't load new window");
		}
		
	
	}
	
	@FXML
	public void openPractiseList () {
		try {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(this.getClass().getResource("/fxml/PractiseList.fxml"));
		StackPane stackPane = loader.load();
		Scene scene = new Scene(stackPane);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.setTitle("Practise List");
		stage.setResizable(false);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
			
		} catch (Exception e) {
			System.out.println("Can't load new window");
	}
	}
}
