package uk.aber.ac.uk;


public class Flashcard {

    private Word flashcard;
    private Lang language;


    public Flashcard(Word word, Lang lang){
        flashcard = word;
        language = lang;
    }

    public String showEnglish(){
        return flashcard.getEnglish();
    }

    public String showWelsh(){
        return flashcard.getWelsh();
    }

    /**
     *
     * @param answer the answer in english or welsh
     * @return if the answer matches the translation
     */
    public boolean isCorrect(String answer){
        if (language==Lang.EN){
            return (answer.equalsIgnoreCase(flashcard.getWelsh()));
        }
        return (answer.equalsIgnoreCase(flashcard.getEnglish()));
    }



}
