package uk.aber.ac.uk;

import java.util.HashSet;

public interface IPracticeSet extends DataStructure{
    HashSet<Word> getList();
    void remove (Word word);
}
