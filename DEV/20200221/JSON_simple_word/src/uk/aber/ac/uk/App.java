package uk.aber.ac.uk;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;


public class App {

    /**
     * generic data structure
     */
    private TreeDictionary dictionary;
    private HashPracticeList practiceList;

    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    /**
     * Runs the main functions of the app
     */

    public void run() {
        dictionary = new TreeDictionary();
        practiceList = new HashPracticeList();
        dictionary = (TreeDictionary) load("dictionary.json", dictionary);
        practiceList = (HashPracticeList) load("practice.json", practiceList);
        save();
    }

    /**
     * Reads the dictionary in to a data structure
     *
     * @param filename file to read
     * @throws IOException file doesn't exist
     */

    public DataStructure load(String filename, DataStructure structure) {
        JSONParser parser = new JSONParser();
        try {
            JSONArray a = (JSONArray) parser.parse(new FileReader(filename));
            for (Object obj : a) {
                JSONObject word = (JSONObject) obj;
                structure.put(word);
            }
        } catch (ParseException e) {
            System.out.println("Empty file");
        } catch (IOException e) {
            System.out.print(filename);
            System.out.println(" Doesn't exist");
        }
        return structure;
    }

    public void save() {
        dictionary.reorder("EN");
        JSONSave("dictionary.json", dictionary.getList());
        JSONSave("practice.json", practiceList.getList());
    }

    private void JSONSave(String filename, Collection<Word> words) {
        JSONArray list = new JSONArray();
        JSONObject JSONWord;
        for (Word word : words) {
            JSONWord = new JSONObject();
            JSONWord.put("english", word.getEnglish());
            JSONWord.put("welsh", word.getWelsh());
            JSONWord.put("wordType", word.getWordType().toString());
            list.add(JSONWord);
        }

        try (FileWriter file = new FileWriter(filename)) {
            file.write(list.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void flashCards() {
        ArrayList<Flashcard> flashcards = new ArrayList<>();
        Lang lang = Lang.EN;
        for (Word card : practiceList.getList()) {
            flashcards.add(new Flashcard(card, lang));
        }
        for (Flashcard card:flashcards){
            System.out.println(card.showEnglish());
            System.out.println(card.showWelsh());
            System.out.println(card.isCorrect(card.showWelsh()));
        }
        for (Flashcard card:flashcards){
            System.out.println(card.isCorrect("P"));
        }
    }


}
