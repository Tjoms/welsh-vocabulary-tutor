package uk.aber.ac.uk;

public class Word {
    private String english;
    private String welsh;
    private Type wordType;

    public Word(String english, String welsh, Type type) {
        this.english = english;
        this.welsh = welsh;
        this.wordType = type;
    }

    public String getEnglish() {
        return english;
    }

    public String getWelsh() {
        return welsh;
    }

    public Type getWordType() {
        return wordType;
    }

    @Override
    public String toString() {
        return "word:{english:" + this.english + " welsh:" + this.welsh + " type:" + this.wordType + "} ";
    }
}
