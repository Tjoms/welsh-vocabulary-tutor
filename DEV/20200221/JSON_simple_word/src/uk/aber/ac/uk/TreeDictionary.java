package uk.aber.ac.uk;

import org.json.simple.JSONObject;

import java.util.*;

public class TreeDictionary implements IDictionary {

    private TreeMap<String, Word> words = new TreeMap<>();
    private Lang ordering = Lang.EN;

    @Override
    public void put(Word word) {
        words.put(word.getEnglish(), word);
    }

    @Override
    public void put(JSONObject word) {
        words.put((String) word.get("english"), new Word((String) word.get("english"), (String) word.get("welsh"), Type.valueOf((String) word.get("wordType"))));
    }

    public void reorder(String language) {
        TreeMap<String, Word> temp = null;
        if (language.equalsIgnoreCase("cy")&&ordering!=Lang.CY) {
            temp = new TreeMap<>();
            for (Map.Entry<String, Word> entry : words.entrySet()) {
                Word word = entry.getValue();
                temp.put(word.getWelsh(), word);
            }
            ordering = Lang.CY;
        }
        if (language.equalsIgnoreCase("en")&&ordering!=Lang.EN) {
            temp  = new TreeMap<>();
            for (Map.Entry<String, Word> entry : words.entrySet()) {
                Word word = entry.getValue();
                temp.put(word.getEnglish(), word);
            }
            ordering = Lang.EN;
        }
        if (temp != null) {
            words = temp;
            System.out.println("BLAH");
        }
    }


    @Override
    public void reverse() {
        TreeMap<String, Word> temp = new TreeMap<>(Collections.reverseOrder());
        temp.putAll(words);
        words = temp;
    }

    public void display() {
        for (Map.Entry<String, Word> entry : words.entrySet()) {
            System.out.println(entry.getValue());
        }
    }

    /**
     * Looks for a word starting with the given input.
     *
     * @param search word/word part to search for
     */
    @Override
    public ArrayList<Word> search(String search) {
        ArrayList<Word> matches = new ArrayList<>();
        words.forEach((s, word) ->
                {
                    if (s.startsWith(search)) {
                        matches.add(word);
                    }

                }
        );
        return matches;
    }


    public Collection<Word> getList() {
        return words.values();
    }
}
