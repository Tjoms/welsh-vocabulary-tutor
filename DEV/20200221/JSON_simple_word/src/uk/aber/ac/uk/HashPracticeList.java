package uk.aber.ac.uk;

import org.json.simple.JSONObject;

import java.util.HashSet;

public class HashPracticeList implements IPracticeSet {

    private HashSet<Word> practiceList = new HashSet<>();

    public HashSet<Word> getList() {
        return practiceList;
    }

    @Override
    public void put(Word word) {
        practiceList.add(word);
    }

    @Override
    public void put(JSONObject word) {
        practiceList.add(new Word((String) word.get("english"), (String) word.get("welsh"), Type.valueOf((String) word.get("wordType"))));
    }

    @Override
    public void display() {
        ;
    }

    @Override
    public void remove(Word word) {
        practiceList.remove(word);
    }
}
