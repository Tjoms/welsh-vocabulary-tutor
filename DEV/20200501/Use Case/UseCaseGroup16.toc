\contentsline {section}{\numberline {1}INTRODUCTION}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Purpose of this Document}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Scope}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Objectives}{2}{subsection.1.3}
\contentsline {section}{\numberline {2}Typical Users}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}User `Oliver\IeC {\textquoteright }}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}User `Sally\IeC {\textquoteright }}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}User `Owen\IeC {\textquoteright }}{2}{subsection.2.3}
\contentsline {section}{\numberline {3}USE CASES}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Use Cases for Typical User}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Use Case 1.1 Reorder dictionary}{3}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Use Case 1.2 Add word to dictionary}{3}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Use Case 1.3 Add word to practice list}{3}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Use Case 1.4 Search in dictionary}{3}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Use Case 1.5 Go to practice list}{3}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Use Case 2.1 Show flashcards}{4}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Use Case 2.2 Run test}{4}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}Use Case 2.3 Remove word from practice list}{4}{subsection.3.9}
\contentsline {section}{\numberline {4}ERROR CONDITIONS}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Error 1.1 Searching for non-existing word}{4}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Error 1.2 Adding a word with wrong information to dictionary list}{5}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Error 1.3 Adding a word to the dictionary list without the necessary information}{5}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Error 2.1 Not enough words for test}{5}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Error 2.2 Not being able to continue the test}{5}{subsection.4.5}
\contentsline {section}{REFERENCES}{5}{subsection.4.5}
\contentsline {section}{DOCUMENT HISTORY}{5}{section*.2}
