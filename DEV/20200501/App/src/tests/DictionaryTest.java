
import main.java.controllers.CommonController;
import main.java.controllers.DictionaryController;
import main.java.dao.Dao;
import main.java.service.Dictionary;
import main.java.service.Lang;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;



public class DictionaryTest {


    //FR-02
    @Test
    public void dictionaryOrderingEnglish() {
        CommonController.userDao.setLang(Lang.EN);
        Dictionary dictionary = CommonController.userDao.getDictionary();

        String firstWord = dictionary.getAsObservable().get(0).getEnglish();
        String lastWord = dictionary.getAsObservable().get(dictionary.getAsObservable().size() - 1).getEnglish();


        Assertions.assertEquals(firstWord.charAt(0), 'a');
        Assertions.assertEquals(lastWord.charAt(0), 'z');
    }

    //FR-04
    @Test
    public void dictionaryOrderingWelsh() {
        CommonController.userDao.setLang(Lang.CY);
        Dictionary dictionary = CommonController.userDao.getDictionary();

        String firstWord = dictionary.getAsObservable().get(0).getWelsh();
        String lastWord = dictionary.getAsObservable().get(dictionary.getAsObservable().size() - 1).getWelsh();


        Assertions.assertEquals(firstWord.charAt(0), 'a');
        int comparison = String.valueOf(lastWord.charAt(0)).compareTo(String.valueOf(firstWord.charAt(0)));
        Assertions.assertTrue(comparison > 0);

    }

    @Test
    public void orderingByLetter() {
        CommonController.userDao.setLang(Lang.CY);
        Dictionary dictionary = CommonController.userDao.getDictionary();
        dictionary.search("r");

        Assertions.assertTrue(dictionary.getAsObservable().get(0).getDisplayWelsh().startsWith("r"));


    }


}