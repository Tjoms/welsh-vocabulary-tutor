package main.java.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import main.java.service.Type;
import main.java.service.Word;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class FlashcardController extends CommonController implements Initializable {
    private int timesFlipped = 0;
    @FXML
    private Button flashcard;
    @FXML
    private Button previousButton;
    @FXML
    private Button goToPractice;
    @FXML
    private Button nextButton;

    private ArrayList<Word> words = new ArrayList<>();
    private int index = 0;

    /**
     * initializes the flashcards with values from the practise list
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (userDao.getPracticeList().getList().size() > 0) {
            words = userDao.getPracticeList().getList();
        } else {
            words.add(new Word("add some words!", "ychwanegu rhai geiriau", Type.other));
        }
    }

    /**
     * flips a flashcard, making it show either welsh or english
     */
    @FXML
    private void flipFlashcard() {
        timesFlipped++;
        Word word = words.get(index);
        if (timesFlipped % 2 == 0) {
            flashcard.setText(word.getEnglish());
        } else if (timesFlipped % 2 == 1) {
            flashcard.setText(word.getWelsh());
        }
    }

    /**
     * increases the index to show the next flashcard
     */
    @FXML
    private void showNextFlashcard() {
        //Go to the next word in the practice list
        index = ++index % words.size();
        flipFlashcard();
    }

    /**
     * decreases the index to show he previous flashcard
     */
    @FXML
    private void showPreviousFlashcard() {
        //Go to the previous word in the practice list
        if (index - 1 > -1) {
            index--;
        }
        flipFlashcard();
    }

    /**
     * goes to the  practice list windows
     *
     * @throws IOException the file might not exist
     */
    @FXML
    private void goToPractice() throws IOException {
        String filePath = "../../resources/fxml/PracticeView.fxml";
        changeScene(filePath, goToPractice);
    }
}
