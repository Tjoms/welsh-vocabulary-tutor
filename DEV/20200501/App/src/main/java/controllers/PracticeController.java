package main.java.controllers;


import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import main.java.service.Word;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PracticeController extends CommonController implements Initializable {
    @FXML
    private Button goToFlashcards;
    @FXML
    private Button goToTest;
    @FXML
    private Button goToDictionary;
    @FXML
    private TableView<Word> tableViewDictionary;
    @FXML
    private TableColumn<Word, String> englishColumn;
    @FXML
    private TableColumn<Word, String> welshColumn;

    /**
     * initializes the practice list window with words from the practice list
     * <p>
     * this controller allows the user to remove words from the practice list, by setting the row factory to delete
     * words by double click
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (checkWords()) {
            goToTest.setDisable(true);
        }

        englishColumn.setCellValueFactory(new PropertyValueFactory<Word, String>("english"));
        welshColumn.setCellValueFactory(new PropertyValueFactory<Word, String>("welsh"));
        tableViewDictionary.setItems(userDao.getPracticeList().getAsObservable());
        tableViewDictionary.setRowFactory(tv -> {
            TableRow<Word> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 2) {
                    userDao.delete(row.getItem());
                    tableViewDictionary.setItems(userDao.getPracticeList().getAsObservable());
                }
            });
            return row;
        });
    }

    /**
     * button which links to the dictionary page
     *
     * @throws IOException file might not exist
     */
    @FXML
    private void goToDictionary() throws IOException {
        String filePath = "../../resources/fxml/DictionaryView.fxml";
        changeScene(filePath, goToDictionary);
    }

    /**
     * button which links to the flashcard page
     *
     * @throws IOException file might not exist
     */
    @FXML
    private void goToFlashcards() throws IOException {
        String filePath = "../../resources/fxml/Flashcard.fxml";
        changeScene(filePath, goToFlashcards);
    }

    /**
     * button which links to the test page (if the practice list has more than 4 words)
     *
     * @throws IOException file might not exist
     */
    @FXML
    private void goToTest() throws IOException {
        if (checkWords()) {
            goToTest.setDisable(true);
        }
        String filePath = "../../resources/fxml/CompareTest.fxml";
        changeScene(filePath, goToTest);
    }

    /**
     * checks if theres more than 4 words in the practice list
     */
    @FXML
    private boolean checkWords() {
        //User should not be able to do tests unless there are at least 4 words in the practice list
        return (userDao.getPracticeList().getList().size() < 4);
    }
}
