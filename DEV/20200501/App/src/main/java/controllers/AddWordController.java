package main.java.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.service.Type;
import main.java.service.Word;

import java.net.URL;
import java.util.ResourceBundle;

public class AddWordController extends CommonController implements Initializable {

    @FXML
    private Button cancel;
    @FXML
    private Button confirm;
    @FXML
    private TextField englishTranslation;
    @FXML
    private TextField welshTranslation;
    @FXML
    private ChoiceBox typeOfWord;


    /**
     * Initializes the add items pop up window by adding the possible word types
     * <p>
     * This controller allows a user to create a new word
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Maybe add genders to noun
        ObservableList<String> typeOfWords = FXCollections.observableArrayList();
        typeOfWords.add("NOUN (MASCULINE)");
        typeOfWords.add("VERB");
        typeOfWords.add("NOUN (FEMININE)");
        typeOfWords.add("ADVERB");
        typeOfWords.add("ADJECTIVE");
        typeOfWord.setItems(typeOfWords);

    }

    /**
     * closes the window, cancelling word creation
     */

    @FXML
    private void closeWindow() {
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }

    /**
     * confirms the new word, updating the backend data structures, practise list and dictionary
     */
    @FXML
    private void confirmAction() {
        Stage stage = (Stage) confirm.getScene().getWindow();
        String english = englishTranslation.getText();
        String welsh = welshTranslation.getText();
        String type = (String) typeOfWord.getValue();
        Word word = new Word(english, welsh, toType(type));
        userDao.updateDictionary(word);
        stage.close();
    }

    /**
     * converts the strings for nouns and verbs into their respective enums
     *
     * @param type type as a string
     * @return type as an enum
     */
    private Type toType(String type) {
        if (type.equalsIgnoreCase("NOUN (MASCULINE)")) {
            return Type.nm;
        }
        if (type.equalsIgnoreCase("NOUN (FEMININE)")) {
            return Type.nf;
        }
        if (type.equalsIgnoreCase("VERB")) {
            return Type.verb;
        }
        return Type.other;
    }
}