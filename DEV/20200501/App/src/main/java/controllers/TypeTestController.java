package main.java.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import main.java.service.RandomGenerator;
import main.java.service.Word;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TypeTestController extends CommonController implements Initializable {
    RandomGenerator randomGenerator = new RandomGenerator();
    Word questionWord = userDao.getPracticeList().getRandomElement();
    String language = "English";
    @FXML
    private Button continueButton;
    @FXML
    private Label questionLabel;
    @FXML
    private TextField answer;


    /**
     * initializes the type test with words from the practice list, randomly deciding whether the question is displayed
     * in welsh or english
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //There is a 50% chance of the word to be displayed in welsh
        if (randomGenerator.chanceOfSuccess(1, 2)) {
            language = "Welsh";
        }
        String word = "";
        if (language.contentEquals("English")) {
            word = questionWord.getWelsh();
        } else if (language.contentEquals("Welsh")) {
            word = questionWord.getEnglish();
        }

        questionLabel.setText("What is " + word + " in " + language);
    }


    /**
     * continues to the next test
     *
     * @throws IOException file might not exist
     */
    @FXML
    private void continueTest() throws IOException {
        if (isAnswerCorrect()) {
            //Add points
        }
        String filePath = "../../resources/fxml/MultipleChoiceTest.fxml";
        changeScene(filePath, continueButton);
    }

    /**
     * checks if the answer is correct
     *
     * @return if the answer is correct or not
     */
    @FXML
    private boolean isAnswerCorrect() {
        if (language.contentEquals("English")) {
            return answer.getText().contentEquals(questionWord.getEnglish());
        } else if (language.contentEquals("Welsh")) {
            return answer.getText().contentEquals(questionWord.getWelsh());

        }
        return false;
    }
}
