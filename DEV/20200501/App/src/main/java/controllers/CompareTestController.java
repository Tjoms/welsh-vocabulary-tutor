package main.java.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import main.java.service.RandomGenerator;
import main.java.service.Word;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CompareTestController extends CommonController implements Initializable {
    @FXML
    private Button continueButton;
    @FXML
    private ChoiceBox choiceBoxA;
    @FXML
    private ChoiceBox choiceBoxB;
    @FXML
    private ChoiceBox choiceBoxC;
    @FXML
    private ChoiceBox choiceBoxD;

    @FXML
    private Label word_A;
    @FXML
    private Label word_B;
    @FXML
    private Label word_C;
    @FXML
    private Label word_D;

    @FXML
    private Label answerSlot_A;
    @FXML
    private Label answerSlot_B;
    @FXML
    private Label answerSlot_C;
    @FXML
    private Label answerSlot_D;

    RandomGenerator randomGenerator = new RandomGenerator();
    String language = "English";

    /**
     * gets the compare test scene and initializes it with words from the practise list
     *
     * this test makes the user match the welsh with the english equivalent
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //There is a 50% chance of the word to be displayed in welsh
        if(randomGenerator.chanceOfSuccess(1,2)){
            language = "Welsh";
        }

        //Careful not to get two equal random words
        //Word should be picked from practice list
        Word wordA = userDao.getPracticeList().getRandomElement();
        Word wordB = userDao.getPracticeList().getRandomElement();
        Word wordC = userDao.getPracticeList().getRandomElement();
        Word wordD = userDao.getPracticeList().getRandomElement();

        ObservableList<String> choicesForChoiceBox = FXCollections.observableArrayList();;

        if(language.contentEquals("English")) {
            setWords(wordA.getEnglish(), wordB.getEnglish(), wordC.getWelsh(), wordD.getEnglish());

            //Should be in random order
            setAnswers(wordA.getWelsh(), wordB.getWelsh(), wordC.getWelsh(), wordD.getWelsh());
            choicesForChoiceBox = choices(wordA.getWelsh(), wordB.getWelsh(), wordC.getWelsh(), wordD.getWelsh());

        } else if(language.contentEquals("Welsh")){
            setWords(wordA.getWelsh(), wordB.getWelsh(), wordC.getWelsh(), wordD.getWelsh());

            //Should be in random order
            setAnswers(wordA.getEnglish(), wordB.getEnglish(), wordC.getEnglish(), wordD.getEnglish());
            choicesForChoiceBox = choices(wordA.getEnglish(), wordB.getEnglish(), wordC.getEnglish(), wordD.getEnglish());
        }

        choiceBoxA.setItems(choicesForChoiceBox);
        choiceBoxB.setItems(choicesForChoiceBox);
        choiceBoxC.setItems(choicesForChoiceBox);
        choiceBoxD.setItems(choicesForChoiceBox);
    }


    private ObservableList<String> choices(String wordA, String wordB, String wordC, String wordD) {
        ObservableList<String> choices = FXCollections.observableArrayList();
        choices.add(wordA);
        choices.add(wordB);
        choices.add(wordC);
        choices.add(wordD);

        return choices;
    }

    private void setAnswers(String wordA, String wordB, String wordC, String wordD) {
        answerSlot_A.setText(wordA);
        answerSlot_B.setText(wordB);
        answerSlot_C.setText(wordC);
        answerSlot_D.setText(wordD);
    }

    private void setWords(String wordA, String wordB, String wordC, String wordD) {
        word_A.setText(wordA);
        word_B.setText(wordB);
        word_C.setText(wordC);
        word_D.setText(wordD);
    }
    /**
     * Continues to the next test
     *
     * @throws IOException file might not exist
     */
    @FXML
    private void continueTest() throws IOException {
        String filePath = "../../resources/fxml/TypeTest.fxml";
        changeScene(filePath, continueButton);
    }
}
