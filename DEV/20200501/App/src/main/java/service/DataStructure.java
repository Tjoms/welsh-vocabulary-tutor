package main.java.service;

import javafx.collections.ObservableList;


/**
 * generic data structure interface, specifies functions required from our data structures for the dictionary
 * <p>
 * this is the factory design pattern
 */

public interface DataStructure {
    void put(Word word);

    ObservableList<Word> getAsObservable();

}
