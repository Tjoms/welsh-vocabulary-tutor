package main.java.service;

import java.util.Objects;

public class Word {
    private String displayWelsh;
    private String displayEnglish;
    private String english;
    private String welsh;
    private Type wordType;

    /**
     * initializes a word
     *
     * @param english english translation
     * @param welsh   welsh translation
     * @param type    type of word
     */
    public Word(String english, String welsh, Type type) {
        this.wordType = type;
        this.english = english;
        this.displayEnglish = toVerb();
        this.welsh = welsh;
        this.displayWelsh = toNoun();
    }

    /**
     * gets the formatted welsh
     *
     * @return formatted welsh (noun type)
     */

    public String getDisplayWelsh() {
        return displayWelsh;
    }

    /**
     * gets the formatted english
     *
     * @return formatted english (run -> to run)
     */

    public String getDisplayEnglish() {
        return displayEnglish;
    }


    /**
     * adds the type of noun to the end of a word
     *
     * @return formatted word (nouns)
     */
    private String toNoun() {

        if (wordType == Type.nf) {
            return welsh + " (Feminine noun)";
        }
        if (wordType == Type.nm) {
            return welsh + " (Masculine noun)";
        }
        return welsh;
    }

    /**
     * adds "to" to the front of verbs
     *
     * @return formatted word (verbs)
     */
    private String toVerb() {
        if (wordType == Type.verb) {
            return "to " + english;
        }
        return english;
    }

    /**
     * returns the english translation
     *
     * @return english translation
     */
    public String getEnglish() {
        return english;
    }

    /**
     * returns the welsh translation
     *
     * @return welsh translation
     */
    public String getWelsh() {
        return welsh;
    }


    /**
     * returns the word object as a string
     *
     * @return word object as a string
     */

    @Override
    public String toString() {
        return "Word{" +
                "displayWelsh='" + displayWelsh + '\'' +
                ", displayEnglish='" + displayEnglish + '\'' +
                ", english='" + english + '\'' +
                ", welsh='" + welsh + '\'' +
                ", wordType=" + wordType +
                '}';
    }

    /**
     * overrides the hashCode function to limit it to include the welsh, english and wordType attributes
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(english, welsh, wordType);
    }


    /**
     * overrides the equals function to limit it to include the welsh, english and wordType attributes
     *
     * @param o an object to compare to
     * @return whether this object and another object are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return Objects.equals(english, word.english) &&
                Objects.equals(welsh, word.welsh) &&
                wordType == word.wordType;
    }

    /**
     * returns words type
     *
     * @return type of word
     */
    public Type getType() {
        return wordType;
    }
}
