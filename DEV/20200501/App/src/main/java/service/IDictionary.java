package main.java.service;


import java.util.ArrayList;

public interface IDictionary extends DataStructure {
    ArrayList<Word> search(String word);
}
