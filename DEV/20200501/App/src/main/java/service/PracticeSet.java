package main.java.service;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class PracticeSet implements IPracticeSet {

    private HashSet<Word> practiceList;

    /**
     * initializes the practice list as an empty hash set
     */
    public PracticeSet() {
        practiceList = new HashSet<>();
    }

    /**
     * adds a word to the practice list
     * @param word
     */
    @Override
    public void put(Word word) {
        practiceList.add(word);
    }

    /**
     * removes a word from the practice list
     *
     * @param word word to remove
     */

    @Override
    public void remove(Word word) {
        practiceList.remove(word);
    }

    /**
     * returns practice list as a array which can be viewed in javafx
     *
     * @return observable array
     */

    public ObservableList<Word> getAsObservable() {
        return FXCollections.observableArrayList(practiceList);
    }

    /**
     * gets a random word from the practice list
     *
     * @return random word
     */
    public Word getRandomElement() {
        if (!practiceList.isEmpty()) {
            int item = new Random().nextInt(practiceList.size());
            return (Word) practiceList.toArray()[item];
        }
        return null;
    }

    /**
     * returns the practice list as a list (convert from hash set)
     *
     * @return practice list
     */
    public ArrayList<Word> getList() {
        return new ArrayList<>(practiceList);
    }


}
