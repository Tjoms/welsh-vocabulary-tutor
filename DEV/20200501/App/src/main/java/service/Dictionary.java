package main.java.service;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.java.service.simple.json.org.JSONArray;
import main.java.service.simple.json.org.JSONObject;
import main.java.service.simple.json.org.parser.JSONParser;
import main.java.service.simple.json.org.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeMap;

public class Dictionary implements IDictionary {

    private TreeMap<String, Word> dictionary;
    private Lang lang;
    private Comparator<String> lowerCompare = String::compareToIgnoreCase;

    /**
     * initializes the dictionary in english and loads it
     */
    public Dictionary() {
        lang = Lang.EN;
        dictionary = load();
    }

    /**
     * loads the dictionary from the json file
     * @return the dictionary as a TreeMap
     */

    private TreeMap<String, Word> load() {
        TreeMap<String, Word> map = new TreeMap<>(lowerCompare);
        JSONParser parser = new JSONParser();
        String dictFile = "src/main/resources/json/dictionary.json";
        try {
            JSONArray a = (JSONArray) parser.parse(new FileReader(dictFile));
            for (Object obj : a) {
                JSONObject word = (JSONObject) obj;
                String english = (String) word.get("english");
                String welsh = (String) word.get("welsh");
                map.put(english.toLowerCase(), new Word(stripChars(english), stripChars(welsh), Type.valueOf((String) word.get("wordType"))));
            }
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            System.out.println("Empty file");
        } catch (IOException e) {
            System.out.println("dictionary file doesn't exist");
        }
        return map;
    }

    /**
     * whitelists all legal characters, if a new character is required, add it to the valid chars regex
     * @param word word to parse
     * @return legal part of the word
     */
    private String stripChars(String word) {
        String validChars = "[^A-Za-z âÂêÊîÎôÔûÛŵŷ’-]";
        return word.split(validChars)[0];
    }

    /**
     * adds a word to the dictionary, removing illegal characters
     * @param word word to add
     */
    @Override
    public void put(Word word) {
        dictionary.put(stripChars(word.getEnglish()), new Word(stripChars(word.getEnglish()), stripChars(word.getWelsh()), word.getType()));
    }

    /**
     * Looks for a word starting with the given input.
     *
     * @param search substring to search for
     */

    @Override
    public ArrayList<Word> search(String search) {
        ArrayList<Word> matches = new ArrayList<>();

        for (Word word : dictionary.values()) {
            {
                if (lang == Lang.EN) {
                    if (word.getEnglish().startsWith(search)) {
                        matches.add(word);
                    }
                } else {
                    if (word.getWelsh().startsWith(search)) {
                        matches.add(word);
                    }
                }
            }
        }
        return matches;
    }

    /**
     * returns the dictionary values as a list which can be displayed in javafx
     * @return
     */

    public ObservableList<Word> getAsObservable() {
        return FXCollections.observableArrayList(dictionary.values());
    }

    /**
     * sets the language of the dictionary, changing the value of the keys to english or welsh depending on the language
     * @param lang language to set the dictionary to
     */

    public void setLang(Lang lang) {
        if (lang != this.lang) {
            this.lang = lang;
            TreeMap<String, Word> map = new TreeMap<>(lowerCompare);
            for (Word word : dictionary.values()) {
                if (lang == Lang.CY) {
                    map.put(word.getWelsh(), word);
                } else {
                    map.put(word.getEnglish(), word);
                }
            }
            dictionary = map;
        }
    }

}
