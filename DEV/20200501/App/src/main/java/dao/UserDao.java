package main.java.dao;

import main.java.service.Dictionary;
import main.java.service.Lang;
import main.java.service.PracticeSet;
import main.java.service.Word;

public class UserDao implements Dao {

    private Dictionary dictionary;
    private PracticeSet practiceList;

    /**
     * initializes the dao with an empty and practice list and a loaded dictionary
     */
    public UserDao() {
        practiceList = new PracticeSet();
        dictionary = new Dictionary();
    }

    /**
     * returns the dictionary object
     * @return dictionary
     */

    @Override
    public Dictionary getDictionary() {
        return dictionary;
    }

    /**
     * returns the practice list object
     * @return practice list
     */

    @Override
    public PracticeSet getPracticeList() {
        return practiceList;
    }

    /**
     * adds a new word, updating the dictionary and adding it to the practice list
     * @param word word to add
     */
    @Override
    public void updateDictionary(Word word) {
        dictionary.put(word);
        practiceList.put(word);
    }

    /**
     * removes a word from the practice list
     * @param word word to remove
     */
    @Override
    public void delete(Word word) {
        practiceList.remove(word);
    }

    /**
     * adds a word to the practice list
     * @param word
     */

    public void favourite(Word word) {
        practiceList.put(word);
    }

    /**
     * sets the dictionaries language ordering
     * @param lang
     */
    public void setLang(Lang lang) {
        dictionary.setLang(lang);
    }
}
