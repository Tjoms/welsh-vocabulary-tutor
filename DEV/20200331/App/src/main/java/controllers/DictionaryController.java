package main.java.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import main.java.service.Lang;
import main.java.service.Type;
import main.java.service.Word;

import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.ResourceBundle;


public class DictionaryController extends CommonController implements Initializable {

    private Lang lang = Lang.EN;
    @FXML
    private TableView<Word> tableViewDictionary;
    @FXML
    private TableColumn<Word, String> englishColumn;
    @FXML
    private TableColumn<Word, String> welshColumn;
    @FXML
    private Button goToPractice;
    @FXML
    private Button addToDictionary;
    @FXML
    private Button goToHelp;
    @FXML
    private TextField search;


    /**
     * A method that initializes everything before the scene is displayed
     *
     * @param url
     * @param resourceBundle
     */

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        englishColumn.setCellValueFactory(new PropertyValueFactory<>("displayEnglish"));
        welshColumn.setCellValueFactory(new PropertyValueFactory<>("displayWelsh"));
        userDao.setLang(lang);
        tableViewDictionary.setItems(userDao.getDictionary().getAsObservable());
        tableViewDictionary.setRowFactory(tv -> {
            TableRow<Word> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 2) {
                    userDao.favourite(row.getItem());
                }
            });
            return row;
        });
        String verbMatch = "to ";
        String nounMatch = "\\(";
        Comparator<String> englishComparator =
                (String v1, String v2) -> {
                    String[] stripped1 = v1.toLowerCase().split(verbMatch);
                    String[] stripped2 = v2.toLowerCase().split(verbMatch);
                    return stripped1[stripped1.length-1].compareTo(
                            stripped2[stripped2.length-1]);
                };
        Comparator<String> welshComparator =
                Comparator.comparing((String v) -> v.toLowerCase().split(nounMatch)[0]);
        welshColumn.setComparator(welshComparator);
        englishColumn.setComparator(englishComparator);
    }

    /**
     * A method that changes the scene where you can add a word to the dictionary
     */

    @FXML
    private void addToDictionary() throws IOException {
        String filePath = "../../resources/fxml/AddWordPopUp.fxml";
        String title = "Add word";
        showPopUp(filePath, title);
        tableViewDictionary.setItems(userDao.getDictionary().getAsObservable());
    }


    /**
     * A method that changes the scene to the help scene
     */
    @FXML
    private void goToHelp() throws IOException {
        String filePath = "../../resources/fxml/HelpView.fxml";
        changeScene(filePath, goToHelp);
    }

    /**
     * A method that changes the scene to the practice scene
     *
     * @throws IOException
     */
    @FXML
    private void goToPractice() throws IOException {
        //should save JSON file before continuing...
        String filePath = "../../resources/fxml/PracticeView.fxml";
        changeScene(filePath, goToPractice);
    }

    /**
     * A method that search in the dictionary and displays the results in the dictionary view
     * If there is nothing in the search bar, all the words in the dictionary is displayed
     */
    @FXML
    private void searchDictionary() {
        userDao.setLang(lang);
        ObservableList<Word> searchedWords = FXCollections.observableArrayList();
        String searchWord = search.getText();
        searchedWords.setAll(userDao.getDictionary().search(searchWord));
        tableViewDictionary.setItems(searchedWords);
    }

    @FXML
    private void onSort(){
        try {
            TableColumn sortColumn = tableViewDictionary.getSortOrder().get(0);
            if (sortColumn.getId().contains("englishColumn")) {
                lang = Lang.EN;
                userDao.setLang(lang);

            } else if (sortColumn.getId().contains("welshColumn")) {
                lang = Lang.CY;
                userDao.setLang(lang);

            }
        }catch (java.lang.RuntimeException ignored){}

    }

}
