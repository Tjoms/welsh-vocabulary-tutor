package main.java.controllers;


import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import main.java.service.Word;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PracticeController extends CommonController implements Initializable {
    @FXML
    private Button goToFlashcards;
    @FXML
    private Button goToTest;
    @FXML
    private Button goToDictionary;
    @FXML
    private TableView<Word> tableViewDictionary;
    @FXML
    private TableColumn<Word, String> englishColumn;
    @FXML
    private TableColumn<Word, String> welshColumn;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        checkWords();

        englishColumn.setCellValueFactory(new PropertyValueFactory<Word, String>("english"));
        welshColumn.setCellValueFactory(new PropertyValueFactory<Word, String>("welsh"));
        tableViewDictionary.setItems(userDao.getPracticeList().getAsObservable());
        tableViewDictionary.setRowFactory(tv -> {
            TableRow<Word> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 2) {
                    userDao.delete(row.getItem());
                    tableViewDictionary.setItems(userDao.getPracticeList().getAsObservable());
                }
            });
            return row;
        });
    }


    @FXML
    private void goToDictionary() throws IOException {
        String filePath = "../../resources/fxml/DictionaryView.fxml";
        changeScene(filePath, goToDictionary);
    }

    @FXML
    private void goToFlashcards() throws IOException {
        String filePath = "../../resources/fxml/Flashcard.fxml";
        changeScene(filePath, goToFlashcards);
    }

    @FXML
    private void goToTest() throws IOException {
        //User should not be able to do tests unless there are at least 4 words in the practice list
        if(userDao.getPracticeList().getList().size() < 4){
            goToTest.setDisable(true);
        }

        String filePath = "../../resources/fxml/CompareTest.fxml";
        changeScene(filePath, goToTest);
    }

    @FXML
    private void checkWords(){
        //User should not be able to do tests unless there are at least 4 words in the practice list
        if(userDao.getPracticeList().getList().size() < 4){
            goToTest.setDisable(true);
        }
    }
}
