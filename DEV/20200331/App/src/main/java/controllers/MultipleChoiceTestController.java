package main.java.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import main.java.service.RandomGenerator;
import main.java.service.Word;

import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class MultipleChoiceTestController extends CommonController implements Initializable {
    @FXML
    private Button continueButton;
    @FXML
    private RadioButton ANSWER1;
    @FXML
    private RadioButton ANSWER2;
    @FXML
    private RadioButton ANSWER3;
    @FXML
    private RadioButton ANSWER4;
    @FXML
    private RadioButton ANSWER5;
    @FXML
    private RadioButton ANSWER6;
    @FXML
    private Label QUESTION;


    RandomGenerator randomGenerator = new RandomGenerator();
    Word questionWord = userDao.getPracticeList().getRandomElement();
    String language = "English";

    public static int generateRandomInt(){
        int x = userDao.getDictionary().getAsObservable().size();
        Random random = new Random();
        return random.nextInt(x);
    }

    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (randomGenerator.chanceOfSuccess(1, 2)) {
            language = "Welsh";
        }
        String word = "";
        if (language.contentEquals("English")) {
            word = questionWord.getWelsh();
        } else if (language.contentEquals("Welsh")) {
            word = questionWord.getEnglish();
        }

        QUESTION.setText("What is " + word + " in " + language);

        Word answer1 = userDao.getDictionary().getAsObservable().get(generateRandomInt());
        Word answer2 = userDao.getDictionary().getAsObservable().get(generateRandomInt());
        Word answer3 = userDao.getDictionary().getAsObservable().get(generateRandomInt());
        Word answer4 = userDao.getDictionary().getAsObservable().get(generateRandomInt());
        Word answer5 = userDao.getDictionary().getAsObservable().get(generateRandomInt());
        Word answer6 = userDao.getDictionary().getAsObservable().get(generateRandomInt());

        if (randomGenerator.getRandomValueFromInterval(1, 6) == 1) {
            answer1 = questionWord;
        } else if (randomGenerator.getRandomValueFromInterval(1, 6) == 2) {
            answer2 = questionWord;
        } else if (randomGenerator.getRandomValueFromInterval(1, 6) == 3) {
            answer3 = questionWord;
        } else if (randomGenerator.getRandomValueFromInterval(1, 6) == 4) {
            answer4 = questionWord;
        } else if (randomGenerator.getRandomValueFromInterval(1, 6) == 5) {
            answer5 = questionWord;
        } else if (randomGenerator.getRandomValueFromInterval(1, 6) == 6) {
            answer6 = questionWord;
        }

        if (language.contentEquals("English")) {
            ANSWER1.setText(answer1.getEnglish());
            ANSWER2.setText(answer2.getEnglish());
            ANSWER3.setText(answer3.getEnglish());
            ANSWER4.setText(answer4.getEnglish());
            ANSWER5.setText(answer5.getEnglish());
            ANSWER6.setText(answer6.getEnglish());

        } else if (language.contentEquals("Welsh")) {
            ANSWER1.setText(answer1.getWelsh());
            ANSWER2.setText(answer2.getWelsh());
            ANSWER3.setText(answer3.getWelsh());
            ANSWER4.setText(answer4.getWelsh());
            ANSWER5.setText(answer5.getWelsh());
            ANSWER6.setText(answer6.getWelsh());
        }
    }

    @FXML
    private void continueTest() throws IOException {
        String filePath = "../../resources/fxml/PracticeView.fxml";
        changeScene(filePath, continueButton);
    }
}
