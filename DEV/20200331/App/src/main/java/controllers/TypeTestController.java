package main.java.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import main.java.service.RandomGenerator;
import main.java.service.Word;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TypeTestController extends CommonController implements Initializable {
    @FXML
    private Button continueButton;

    @FXML
    private Label questionLabel;
    @FXML
    private TextField answer;

    RandomGenerator randomGenerator = new RandomGenerator();
    Word questionWord = userDao.getPracticeList().getRandomElement();
    String language = "English";

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //There is a 50% chance of the word to be displayed in welsh
        if(randomGenerator.chanceOfSuccess(1,2)){
            language = "Welsh";
        }
        String word = "";
        if(language.contentEquals("English")) {
            word = questionWord.getWelsh();
        }else if(language.contentEquals("Welsh")){
            word = questionWord.getEnglish();
        }

        questionLabel.setText("What is " + word + " in " + language);
    }

    @FXML
    private void continueTest() throws IOException {
        if(isAnswerCorrect()){
            //Add points
        }
        String filePath = "../../resources/fxml/MultipleChoiceTest.fxml";
        changeScene(filePath, continueButton);
    }

    @FXML
    private boolean isAnswerCorrect(){
        boolean isAnswerCorrect = false;
        if(language.contentEquals("English")){
            if(answer.getText().contentEquals(questionWord.getEnglish())){
                isAnswerCorrect = true;
            }
        } else if(language.contentEquals("Welsh")){
            if(answer.getText().contentEquals(questionWord.getWelsh())){
                isAnswerCorrect = true;
            }
        }
        if(isAnswerCorrect){
            System.out.println(answer.getText() + " is correct");
        } else{
            System.out.println(answer.getText());
        }

        return isAnswerCorrect;
    }
}
