package main.java.controllers;

import main.java.service.Type;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.service.Word;


import java.net.URL;
import java.util.ResourceBundle;

public class AddWordController extends CommonController implements Initializable {

    @FXML
    private Button cancel;
    @FXML
    private Button confirm;
    @FXML
    private TextField englishTranslation;
    @FXML
    private TextField welshTranslation;
    @FXML
    private ChoiceBox typeOfWord;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Maybe add genders to noun
        ObservableList<String> typeOfWords = FXCollections.observableArrayList();
        typeOfWords.add("NOUN (MASCULINE)");
        typeOfWords.add("VERB");
        typeOfWords.add("NOUN (FEMININE)");
        typeOfWords.add("ADVERB");
        typeOfWords.add("ADJECTIVE");
        typeOfWord.setItems(typeOfWords);

    }

    @FXML
    private void closeWindow() {
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void confirmAction() {
        Stage stage = (Stage) confirm.getScene().getWindow();
        String english = englishTranslation.getText();
        String welsh = welshTranslation.getText();
        String type = (String) typeOfWord.getValue();
        Word word = new Word(english, welsh, toType(type));
        userDao.updateDictionary(word);

        //should save to JSON and update practice list.
        stage.close();
    }

    private Type toType(String type) {
        if (type.equalsIgnoreCase("NOUN (MASCULINE)")) {
            return Type.nm;
        }
        if (type.equalsIgnoreCase("NOUN (FEMININE)")) {
            return Type.nf;
        }
        if (type.equalsIgnoreCase("VERB")) {
            return Type.verb;
        }
        return Type.other;
    }
}