package main.java.controllers;

import main.java.dao.Dao;
import main.java.dao.UserDao;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;


import java.io.IOException;

public abstract class CommonController {

    static public Dao userDao = new UserDao();

    /**
     * A method that changes the scene
     *
     * @param filePath is the path to to the new file
     * @param b        is the button used to overwrite the current scene
     * @throws IOException if the filepath is wrong
     */

    @FXML
    public void changeScene(String filePath, Button b) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource(filePath));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void showPopUp(String filePath, String title) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(filePath));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(scene);
        stage.show();
    }


    /**
     * A method that returns a list of words.
     * Should get words from the dictionary (JSON)
     *
     * @return a list of all the words
     */

}
