package main.java.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HelpController extends CommonController implements Initializable {
    @FXML
    private Button goToDictionary;
    @FXML
    private Label helpInfo;
    @FXML
    private TreeView<String> helpView;
    //needs to get some information displayed (currently navigation method: Tree (Empty))


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        TreeItem<String> main = new TreeItem<>("Welsh Vocabulary Tutor");
        TreeItem<String> dictionary = new TreeItem<>("Dictionary");
        TreeItem<String> practice = new TreeItem<>("Practice");
        TreeItem<String> tests = new TreeItem<>("Test");

        main.getChildren().add(dictionary);
        main.getChildren().add(practice);

        practice.getChildren().add(tests);

        helpView.setRoot(main);

    }

    @FXML
    private void displayHelp(){

//        System.out.println(helpView.getEditingItem().getValue());
//        helpInfo.setText(helpView.getEditingItem().getValue());
    }
    @FXML
    private void goToDictionary() throws IOException {
        String filePath = "../../resources/fxml/DictionaryView.fxml";
        changeScene(filePath, goToDictionary);
    }

}
