package main.java.service;


import main.java.service.simple.json.org.JSONObject;

import java.util.ArrayList;

public interface IDictionary extends DataStructure {
    ArrayList<Word> search(String word);
}
