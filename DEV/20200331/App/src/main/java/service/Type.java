package main.java.service;

/**
 * Possible types of words
 * <p>
 * nm = noun masculine
 * nf = noun feminine
 * v/V = verb
 */
public enum Type {
    nm, verb, nf, other
}
