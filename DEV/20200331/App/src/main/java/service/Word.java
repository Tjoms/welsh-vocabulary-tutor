package main.java.service;

import java.util.Objects;

public class Word {
    private String displayWelsh;
    private String displayEnglish;
    private String english;
    private String welsh;
    private Type wordType;

    public Word(String english, String welsh, Type type) {
        this.wordType = type;
        this.english = english;
        this.displayEnglish = toVerb();
        this.welsh = welsh;
        this.displayWelsh = toNoun();
    }

    public String getDisplayWelsh() {
        return displayWelsh;
    }

    public String getDisplayEnglish() {
        return displayEnglish;
    }

    private String toNoun() {

        if (wordType == Type.nf){
            return welsh + " (Feminine noun)";
        }
        if (wordType == Type.nm){
            return welsh + " (Masculine noun)";
        }
        return welsh;
    }

    public String getEnglish() {
        return english;
    }

    public String getWelsh() {
        return welsh;
    }

    private String toVerb() {
        if (wordType == Type.verb) {
            return "to " + english;
        }
        return english;
    }


    @Override
    public String toString() {
        return "Word{" +
                "displayWelsh='" + displayWelsh + '\'' +
                ", displayEnglish='" + displayEnglish + '\'' +
                ", english='" + english + '\'' +
                ", welsh='" + welsh + '\'' +
                ", wordType=" + wordType +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(english, welsh, wordType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return Objects.equals(english, word.english) &&
                Objects.equals(welsh, word.welsh) &&
                wordType == word.wordType;
    }

    public Type getType() {
        return wordType;
    }
}
