package main.java.service;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.java.service.simple.json.org.JSONObject;

import java.util.*;

public class PracticeSet implements IPracticeSet {

    private HashSet<Word> practiceList;

    public PracticeSet() {
        practiceList = new HashSet<>();
    }

    @Override
    public void put(Word word) {
        practiceList.add(word);
    }

    @Override
    public void remove(Word word) {
        practiceList.remove(word);
    }

    public ObservableList<Word> getAsObservable() {
        return FXCollections.observableArrayList(practiceList);
    }

    public Word getRandomElement() {
        if (!practiceList.isEmpty()) {
            int item = new Random().nextInt(practiceList.size());
            return (Word) practiceList.toArray()[item];
        }
        return null;
    }

    public ArrayList<Word> getList() {
        return new ArrayList<>(practiceList);
    }



}
