package main.java.service;

import java.util.Random;

public class RandomGenerator {

    Random random = new Random();
    /**
     * A method used to get a chance, true or false.
     * Chance = numerator/denominator
     *
     * @param numerator the numerator of the chance
     * @param denominator the denominator of the chance
     * @return true if you are lucky.
     */
    public boolean chanceOfSuccess(int numerator, int denominator) {
        //Example using 3 as numerator and 5 as denominator
        /*
         *multiplied by 1000 to get more values and make
         *the values gathered from .nextInt more random,
         *since the function does not choose a random number perfectly
         *
         * From an interval [0 .. 5] to [0 .. 5000]
         */
        int randomValue = random.nextInt(denominator * 1000);
        /*
         *To make sure we have a defined range of numbers we can work with.
         *The each value in range will have 1000 possible values.
         */
        int valueFromRange = randomValue % denominator;
        /*
         *To get chance wanted, we complete the below statement
         *in the same number of times as the numerator because:
         *
         *These are 3 different pieces making up 3/5
         * P(A|B|C) = P(A) + P(B) + P(C) = 1/5 + 1/5 + 1/5 = 3/5
         *
         * we can NOT compare the 1/5 random value to a constant 3 times because the chance would then be:
         * P(A∩B∩C) = 1/5*1/5*1/5 = 1/15
         *
         */
        for (int currentNumeratorValue = 0; currentNumeratorValue < numerator; currentNumeratorValue++) {
            //The 1/denominator chance compared to one constant value in the range.
            //Since every range starts from 0, we will start using 0 to compare
            if (valueFromRange == currentNumeratorValue) {
                return true;
            }
        }
        return false;
    }


    /**
     * A method that returns a random number from a randomValueFromInterval defined by a lower bound and an upper bound
     *
     * @param lowerBound the lowest value from the interval
     * @param upperBound the highest value from the interval
     * @return a integer from the interval: [lowerBound .. upperBound] ⇒ {x ∈ N: lowerBound ≤ x ≤ upperBound}
     */
    public int getRandomValueFromInterval(int lowerBound, int upperBound) {
        int range = upperBound - lowerBound;
        /*
         *Range +1 because we want to count the number we are starting on
         *example:
         *[5 .. 10] = {5, 6, 7, 8, 9, 10} -> 6 numbers
         *10-5 = 5, then add 1 -> 6
         */
        range += 1;
        /*
         *Multiplied by 1000 to get more values and make
         *the values gathered more random since the .nextInt
         *function does not choose a random number perfectly
         */
        int randomValue = random.nextInt(range * 1000);
        /*
         *We want a value from one number to another and not exceed it,
         *therefore we take randomValue%range. We also want the value to start
         *from the lower bound of the Interval, therefore we also need to add
         *the lower bound.
         *{x ∈ N: lowerBound ≤ x ≤ upperBound}
         */

        return (randomValue % range) + lowerBound;
    }
}
