package main.java.dao;


import main.java.service.Dictionary;
import main.java.service.Lang;
import main.java.service.PracticeSet;
import main.java.service.Word;

public interface Dao {
    Dictionary getDictionary();
    PracticeSet getPracticeList();
    void updateDictionary(Word word);
    void delete(Word word);
    void favourite(Word word);
    void setLang(Lang en);
}
