package main.java.dao;

import main.java.service.*;

public class UserDao implements Dao {

    private Dictionary dictionary;
    private PracticeSet practiceList;

    public UserDao() {
        practiceList = new PracticeSet();
        dictionary = new Dictionary();
    }

    @Override
    public Dictionary getDictionary() {
        return dictionary;
    }

    @Override
    public PracticeSet getPracticeList() {
        return practiceList;
    }

    @Override
    public void updateDictionary(Word word) {
        dictionary.put(word);
        practiceList.put(word);
    }

    @Override
    public void delete(Word word) {
        practiceList.remove(word);
    }

    public void favourite(Word word) {
        practiceList.put(word);
    }

    public void setLang(Lang lang){
        dictionary.setLang(lang);
    }
}
