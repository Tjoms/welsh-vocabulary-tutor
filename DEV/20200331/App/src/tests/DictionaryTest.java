package tests;

import main.java.controllers.CommonController;
import main.java.controllers.DictionaryController;
import main.java.dao.Dao;
import main.java.service.Dictionary;
import main.java.service.Lang;
import org.junit.Assert;
import org.junit.Test;


public class DictionaryTest {


        @Test
        public void dictionaryOrderingEnglish(){
                CommonController.userDao.setLang(Lang.EN);
                Dictionary dictionary=CommonController.userDao.getDictionary();

        String firstWord=dictionary.getAsObservable().get(0).getEnglish();
        String lastWord=dictionary.getAsObservable().get(dictionary.getAsObservable().size()-1).getEnglish();


        Assert.assertEquals(firstWord.charAt(0),'a');
        Assert.assertEquals(lastWord.charAt(0),'z');
        }

        @Test
        public void dictionaryOrderingWelsh(){
                CommonController.userDao.setLang(Lang.CY);
                Dictionary dictionary=CommonController.userDao.getDictionary();

                String firstWord=dictionary.getAsObservable().get(0).getWelsh();
                String lastWord=dictionary.getAsObservable().get(dictionary.getAsObservable().size()-1).getWelsh();


                Assert.assertEquals(firstWord.charAt(0),'a');
               int comparison= String.valueOf(lastWord.charAt(0)).compareTo(String.valueOf(firstWord.charAt(0)));
                Assert.assertTrue(comparison>0);

        }







}